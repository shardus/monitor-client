# @shardus/monitor-client

## Releasing

If you're daring and want to cut and push a release to npm, you can do so
solely by running `npm run release`. But I warn you - with great power, comes
great responsibility ;)
